��          �                         A   -  -   o  (   �     �     �     �     �     �  0   	  3   :     n     �  S  �     �     �  k     -   �  ,   �     �     �     �     �       0     3   P     �     �   %s Settlement %s cannot be used %s cannot be used when multiple shipping addresses are specified. Changes between test and production versions. Click here for more information about %s Disable Enable Environment Production environment Test environment The currency code %1$s is not supported by %2$s. Thoroughly test your integration before going live. Update %s Settings enable/disable %s Project-Id-Version: Aivec LLC.
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2021-01-14T18:19:27+09:00
PO-Revision-Date: 2018-11-21 22:15+0900
Language: en
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.11
 %s Settlement %s cannot be used at this time. %s cannot be used when multiple shipping addresses are specified. Please choose a different payment method. Changes between test and production versions. Click here for more information about %s 》 Disable Enable Environment Production environment Test environment The currency code %1$s is not supported by %2$s. Thoroughly test your integration before going live. Update %s Settings enable/disable %s 